var VALID_SUB_TYPES = [1,2,3,"B",5,6,7,8,"D",10,11,"P",13,"M",15,16,17,18,19,20];
var VALID_LENGTH_AND_MULTIPLIERS = {
	12:1,
	6:1.1,
	3:1.15
};
var VALID_SUB_PROMO_CARD_TYPES = ["SUB-B", "SUB-D", "SUB-P", "SUB-M"];

subscription_data_bus = new Vue({
	data: {
		'subscription': new Subscription(sub_data), //Vue can't react directly to this because it "doesn't" exist so
		// it can't create getters/setters for observation even though data changes
		'sub_types': VALID_SUB_TYPES,
		'sub_lengths': VALID_LENGTH_AND_MULTIPLIERS,
		//having this directly here allows vue to establish reactivity for this data object
		'summary': {
			'price': 0,
			'name': "",
			selected_card_last_four: "0000",
			start_date: '',
			step: 0,
			sub_type: 'B',
			todays_payment: 0,
			applied_promo_code: ''
		}
	},
	created: function() {
		this.subscription.set_sub_type(this.sub_types[0]);
		this.subscription.set_sub_length(Object.keys(this.sub_lengths)[0]);
		this.subscription.set_sub_name(subscription_display_names[this.sub_types[0]]);
		
		var date = new Date();
		
		//add the one to month because date month is zero indexed
		this.subscription.start_date = (date.getMonth() + 1)  + "/" + date.getDate() + "/" + date.getFullYear();
		
		//set summary data with the defaults
		this.update_summary();
	},
	methods: {
		update_summary: function() {
			this.summary.price = this.subscription.monthly_payment;
			this.summary.name = this.subscription.sub_name;
			this.summary.start_date = this.subscription.start_date;
			this.summary.todays_payment = this.subscription.currency_symbol;
			
			if(this.subscription.prepaid)
			{
				this.summary.todays_payment += this.subscription.todays_prepaid_payment;
			}
			else
			{
				this.summary.todays_payment += this.subscription.todays_payment;
			}
			
		}
	}
});

communication_bus = new Vue();

Vue.component('app-viewer', {
	template: '<div>' +
		          '<progress-bar @active_step="update_step"></progress-bar>' +
		          '<div class="min-height-264-px" v-show="step == 0">' +
	                   '<detail-view class="height-225-px" v-show="step == 0"></detail-view>' +
	                   '<drop-down-select-length></drop-down-select-length>' +
			           '<drop-down-select-plan id="sub_plan"></drop-down-select-plan>' +
		          '</div>' +
		          '<user_subscription_info class="min-height-264-px" v-show="step == 1"></user_subscription_info>' +
		          '<billing_payment class="min-height-264-px" v-show="step == 2"></billing_payment>' +
		          '<review-confirm class="min-height-264-px" v-show="step == 3"></review-confirm>' +
		          '<div class="row">' +
	                   '<button class="col-xs-2 fa fa-chevron-left btn btn-primary" @click="prev_step" :disabled="step' +
	          ' ==' +
	          ' min_step' +
	          ' ? true :' +
	          ' false"></button>' +
	                   '<button class="col-xs-2 col-xs-push-8 fa fa-chevron-right btn btn-primary" @click="next_step" :disabled="step == max_step ? true' +
	          ' :' +
	          ' false"></button>' +
	              '</div>' +
	          '</div>',
	data: function() {
		return {
			step: 0,
			data_bus: subscription_data_bus.summary,
			max_step: 3,
			min_step: 0
		}
	},
	methods: {
		update_step: function(step) {
			this.step = step;
		},
		next_step: function() {
			var next_step = this.step + 1;
			
			if(next_step > this.max_step)
			{
				next_step = this.max_step;
			}
			
			communication_bus.$emit('update_progress_bar', next_step);
		},
		prev_step: function() {
			var prev_step = this.step - 1;
			
			if(prev_step < this.min_step)
			{
				prev_step = this.min_step;
			}
			
			communication_bus.$emit('update_progress_bar', prev_step);
		}
	}
});

Vue.component('progress-bar', {
	template: '<div>' +
		          '<h4 id="step-title" class="text-center silhouette-blue-text">{{title}}</h4>' +
			          '<div class="step-bar-container text-center">' +
			                '<progress-bar-step v-for="(step, index) in steps_data" :show="show_bar(index)" ' +
	                        ':details="step.step_details" :step="step.step_order"' +
	                        ' :active="step.is_active" ' +
	                        ' @step_number="update_progress_bar"></progress-bar-step>' +
	                   '</div>' +
				'<div class="step-names text-center">' +
		            '<p v-for="step in steps_data">{{step.name}}</p>' +
				'</div>' +
	          '</div>',
	data: function(){
		return {
			'steps': [],
			'title': this.name,
			data_bus: subscription_data_bus.summary,
			active_step: 0
		}
	},
	created: function() {
		this.steps = this.$children;
		self = this;
		this.active_step = communication_bus.$on('update_progress_bar', function(step){
			self.update_progress_bar(step);
		})
	},
	methods:{
		show_bar: function(index) {
			return index < Object.keys(this.steps_data).length -1
		},
		update_progress_bar: function(step_number, name) {
			this.active_step = step_number;
			
			for(var i = 0; i < this.steps.length; i++)
			{
				//Set the states to false so that it clears the progress bar when going back
				this.steps[i].is_active = false;
				this.steps[i].is_complete = false;
				
				//fills the progress bar
				if(i < step_number)
				{
					this.steps[i].is_complete = true;
				}
				
				//highlights active step in progress bar
				if(i === step_number)
				{
					this.steps[i].is_active = true;
					
					if(typeof name == 'undefined')
					{
						name = (this.steps[i].details == 'undefined') ? '' : this.steps[i].details;
					}
				}
			}
			
			this.title = name.toUpperCase();
			
			this.$emit('active_step', this.active_step);
			
			return this.active_step;
		}
	},
	props: {
		steps_data: {
			type: Array,
			default: function() {
				return [
							{
								name: "PLANS",
								step_details: translator.get('section-title-choose-subscription'),
								step_order: 0,
								is_active: true
							},
							{
								name: "START DATE",
								step_details: translator.get('section-title-start-date'),
								step_order: 1,
								is_active: false
							},
							{
								name: "PAYMENT",
								step_details: translator.get('section-title-payment-and-billing'),
								step_order: 2,
								is_active: false
							},
							{
								name: "CONFIRM",
								step_details: translator.get('section-title-review-confirm'),
								step_order: 3,
								is_active: false
							}
						]
			}
		},
		name: {
			type: null,
			default: translator.get('section-title-choose-subscription').toUpperCase()
		}
	}
});

Vue.component('progress-bar-step', {
	template: '<div class="inline" @click="adjust_selection">' +
		          '<div class="inline step-bar step-bar-node" :class="{\'active-node\' : is_active, \'completed-node\' : is_complete}"></div>' +
		          '<div v-if="this.show" class="inline step-bar step-bar-segment" :class="{\'completed-section\' : is_complete}"></div>' +
	          '</div>',
	data: function(){
		return {
			'is_active': this.active,
			'is_complete': false
		}
	},
	props:
		{
			show: {
				type: Boolean,
				default: true
			},
			details: {
				type: String,
				default: ""
			},
			step: {
				type: Number,
				default: 0
			},
			active: {
				type: Boolean,
				default: false
			}
		},
	methods: {
		adjust_selection: function() {
			this.$emit('step_number', this.step, this.details);
		}
	}
});

Vue.component('drop-down-select-length', {
	computed: {
		parsed_options: function() {
			var parsed_options = [];
			var keys = Object.keys(this.options);
			for(var i = 0; i < keys.length; i++)
			{
				parsed_options.push(
					{
						name: translator.get(keys[i] + '-month-label'),
						value: parseInt(keys[i])
					}
				);
			}
			
			return parsed_options;
		}
	},
	template: '<select class="width-50-percent" @change="selection_made" v-model="selected_value">' +
		       '<option ' +
			       'v-for="option in parsed_options" ' +
			       ':value="option.value" ' +
			       '>' +
			            '{{option.name}}' +
		       '</option>' +
	       '</select>',
	data: function(){
		return {
			selected_value: 12,
			options: []
		}
	},
	methods:
		{
			selection_made: function() {
				subscription_data_bus.subscription.set_sub_length(this.selected_value);
				subscription_data_bus.update_summary();
			}
		},
	created: function(){
		this.options = subscription_data_bus.sub_lengths;
		subscription_data_bus.subscription.set_sub_length(this.selected_value);
		subscription_data_bus.update_summary();
		
	}
});

Vue.component('drop-down-select-plan', {
	props: {
		id: {
			required: true
		}
	},
	template: '<select class="width-50-percent" :id="id" @change="selection_made" v-model="summary.sub_type">' +
			       '<option ' +
				       'v-for="option in option_order" ' +
				       ':value="option" ' +
				       '>' +
			                '{{option_data[option].subscriptionName}}<span v-if="option_data[option].subscriptionName == \'Level\'"> {{option_data[option].subscriptionType}}</span>' +
			       '</option>' +
	            '</select>',
	data: function(){
		return {
			summary: subscription_data_bus.summary,
			option_order: [],
			data_bus: subscription_data_bus.subscription
		}
	},
	methods:{
		selection_made: function() {
			this.data_bus.set_sub_type(this.summary.sub_type);
			this.data_bus.set_sub_name(subscription_display_names[this.summary.sub_type]);
			subscription_data_bus.update_summary();
		}
	},
	created: function() {
		this.option_data = sub_data;
		this.option_order = subscription_data_bus.sub_types;
		this.data_bus.set_sub_type(this.summary.sub_type);
		this.data_bus.set_sub_name(subscription_display_names[this.summary.sub_type]);
		subscription_data_bus.update_summary();
		
	}
});

Vue.component('detail-view',{
	template: '<div class="small-blue-frame">' +
	            '<ul class="no-bullets small-list-offset">' +
	                '<li v-for="detail in details">' +
	                    '<p class="no-margins" v-html="detail.name"></p> ' +
                        '<ul class="no-bullets small-list-offset" v-if="detail.sub_details.length > 0">' +
	                        '<li v-for="sub_detail in detail.sub_details">' +
	                            '<p class="no-margins" v-html="sub_detail"></p>' +
                            '</li>' +
	                    '</ul>' +
	                '</li>' +
	            '</ul>' +
	            '<span class="xs-text" v-html="fine_print"></span>' +
	          '</div>',
	data: function() {
		return {
			data_bus: subscription_data_bus.subscription,
			summary: subscription_data_bus.summary
		}
	},
	computed:{
		details: function() {
			var details_array = [] ;
			
			var monthly_price = this.summary.price;
			
			details_array.push({ name: "Plan Name: " + this.data_bus.sub_name, sub_details: [] });
			details_array.push({ name: translator.get('plan-price-label') + ": " + currency_format(monthly_price, currency_code, currency_symbol) + " per month", sub_details: [] });
			details_array.push({ name: translator.get('monthly-credits-label') + ": " + currency_format(this.data_bus.allowance, currency_code, currency_symbol) + " per month" , sub_details: [] });
			var features_array = this.data_bus.features.slice(0);
			details_array.push({ name: translator.get('plan-features-label') + ": ", sub_details: features_array });
			details_array[3].sub_details.push(translator.get('sub-avg-price-download', {'@@cost': this.data_bus.cost_per_download}));
			
			return details_array;
		},
		fine_print: function() {
			return translator.get('most-downloads-cost', {'@@cost': currency_format(this.data_bus.average_single_item_cost, currency_code, currency_symbol)});
		}
	},
	created: function()
	{
		var this_component = this;
		this.$on('subscription-length-changed', function(length){
			this_component.update_details(length);
		});
	}
});

Vue.component('billing_payment', {
	template:   '<div>' +
	                'Click <a data-toggle="modal" data-target="#promo-modal">here</a> to apply a' +
	                ' code.' +
	                '<div class="modal fade" id="promo-modal">' +
	                    '<div class="modal-dialog">' +
	                        '<div class="modal-content">' +
		                        '<div class="modal-body">' +
				                    '<p>' + translator.get('promo-code-info') + '</p>' +
				                    '<input type="text" class="promo-input" v-model="promo_code">' +
				                    '<button @click="apply_code" class="btn-primary">' + translator.get('apply-promo-btn-text').toUpperCase() + '</button>' +
			                    '</div>' +
					            '<div class="modal-footer">' +
					                '<button type="button" class="btn btn-default"' +
	                                ' data-dismiss="modal">Close</button>' +
					            '</div>' +
		                    '</div>' +
	                    '</div>' +
	                '</div>' +
	                '<div>' +
	                    '<p class="sm-text">' + translator.get('how-to-pay-text') + '</p>' +
	                    '<div>' +
		                    '<input class="float-left" type="radio" name="payment-type" id="payment-monthly" value="false"' +
	                        ' v-model="prepaid" @click="update_payment_plan">' +
		                    '<label class="width-90-per sm-text" for="payment-monthly"' +
	                        ' v-html="monthly_payment"></label>' +
		                '</div>' +
		                '<div>' +
		                    '<input class="float-left" type="radio" name="payment-type" id="payment-prepaid" value="true"' +
	                        ' v-model="prepaid" @click="update_payment_plan">' +
		                    '<label class="width-90-per sm-text" for="payment-prepaid"' +
	                        ' v-html="upfront_payment"></label>' +
	                    '</div>' +
                    '</div>' +
                    '<div>' +
	                    '<select v-model="data_bus.card_id" @change="update_selected_card_data"' +
						' v-show="!data_bus.pay_with_paypal">' +
	                        '<option value="0">' + translator.get('select-card') + '...</option>' +
	                        '<option v-for="card in user_cards" :value="card.id">{{card.cardType}} -' +
	                        ' ************{{card.lastFour}} - Exp: {{card.expMonth}}/{{card.expYear}}</option>' +
	                    '</select>' +
	                    '<div v-show="data_bus.pay_with_paypal" class="bg-info padding-xs sm-text">' +
	                         translator.get('confirm-paypal-request') +
	                    '</div>' +
	                    '<div class="margin-top-sm">' +
		                    '<button @click="add_card" v-show="!data_bus.pay_with_paypal" class="btn btn-primary" data-toggle="modal" data-target="#card-modal">' + translator.get('new-card-btn-text').toUpperCase() + '</button>' +
				            '<div class="modal fade" id="card-modal">' +
					            '<div class="modal-dialog">' +
						            '<div class="modal-content">' +
							            '<div class="modal-body no-padding" v-html="iframe_code">' +
							            '</div>' +
								            '<div class="modal-footer">' +
								            '<button type="button" class="btn btn-default"' +
								            ' data-dismiss="modal" @click="destroy_iframe">Close</button>' +
							            '</div>' +
						            '</div>' +
					            '</div>' +
				            '</div>' +
			                '<button @click="pay_with_card" v-show="data_bus.pay_with_paypal" class="btn' +
	                        ' btn-default">Pay With Card</button>' +
		                    '<button @click="pay_with_paypal" class="margin-left-md btn" v-show="prepaid && !data_bus.pay_with_paypal">Pay with PayPal</button>' +
	                    '</div>' +
	                '</div>' +
	                '<div class="margin-top-xl text-center">' +
	                    translator.get('todays-payment-label') + '<span class="silhouette-blue-text bold lg-text"' +
	                    ' v-html="summary.todays_payment"></span>' +
	                '</div>' +
	            '</div>',
	created: function() {
		this.get_user_cards();
	},
	computed: {
		monthly_payment: function() {
			var todays_payment = currency_format(this.data_bus.todays_payment, currency_code, currency_symbol);
			var price = currency_format(this.data_bus.price, currency_code, currency_symbol);
			return 'Monthly: ' + todays_payment + ' today and 12 monthly payments of ' + price + ' starting on ' +
			this.data_bus.start_date;
		},
		upfront_payment: function() {
			
			var pre_paid = currency_format(this.data_bus.todays_prepaid_payment, currency_code, currency_symbol);
			
			return 'Up-front: Pay one payment of ' + pre_paid + ' today and save 5%';
		},
		iframe_code: function(){
			var html = '';
			
			if(this.show_iframe)
			{
				html = '<iframe src="https://local.sds.com/edit-card" class="height-360-px width-100-percent"></iframe>'
			}
			
			return html;
		}
	},
	data: function() {
		return {
			data_bus: subscription_data_bus.subscription,
			message: '',
			prepaid: false,
			promo_code: "",
			user_cards: {},
			show_iframe: false,
			summary: subscription_data_bus.summary
		}
	},
	methods: {
		add_card: function() {
			this.show_iframe = true;
		},
		apply_code: function() {
			var response = this.data_bus.validate_promo_code(this.promo_code, user_data['userId']);
			
			if(response.is_valid)
			{
				this.message = "Code Applied";
				
				if(VALID_SUB_PROMO_CARD_TYPES.indexOf(response.code_type) > -1){
					//If the promo specifies the type of subscription switch to that type
					this.data_bus.set_subscription_card(this.promo_code, response.code_type);
				}
				else{
					var promo_multiplier = 1;
					if(response.hasOwnProperty(this.summary.sub_type))
					{
						if(response.std_discount > response[this.summary.sub_type].sub_discount)
						{
							promo_multiplier = response[this.summary.sub_type].sub_discount;
						}
						else
						{
							promo_multiplier = response.std_discount;
						}
					}
					else
					{
						promo_multiplier = response.std_discount;
					}
					this.data_bus.set_promo_multiplier(promo_multiplier);
					this.data_bus.set_promo_code(this.promo_code, response.code_type);
					
				}
				
				this.data_bus.set_auto_renew(false);
			}
			else
			{
				this.message = "The supplied code has been used or is not valid";
			}
			
			subscription_data_bus.update_summary();
			
		},
		destroy_iframe: function() {
			this.show_iframe = false;
			this.get_user_cards();
		},
		pay_with_card: function() {
			this.data_bus.pay_with_paypal = false;
			subscription_data_bus.update_summary();
		},
		pay_with_paypal: function() {
			this.data_bus.pay_with_paypal = true;
			subscription_data_bus.update_summary();
		},
		update_payment_plan: function() {
			this.prepaid = (this.prepaid == 'true');
			this.data_bus.set_prepaid_state(this.prepaid);
			
			if(!this.prepaid)
			{
				this.pay_with_card();
			}
			
			subscription_data_bus.update_summary();
		},
		update_selected_card_data: function () {
			this.summary.selected_card_last_four = this.user_cards[this.data_bus.selected_card].lastFour;
		},
		get_user_cards: function(){
			
			var component = this;
			
			$.ajax({
				url: ajaxURL + "?action=",
				type: "POST",
				dataType: "json",
				async: false,
				success: function(result){
					component.user_cards = result;
					console.log(result);
				},
				error: function(xhr, text, err){
					displayMessage(err + '(' + xhr + ')' + ' ' + text);
				}
				
			});
		}
	}
});

Vue.component('user_subscription_info',{
	template: '<div class="padding-left-md">' +
	                '<simple-table :table_data="subscriptions_info_table"></simple-table>' +
	                '<div v-show="this.subscription.is_upgrade">' +
	                    '<input class="float-left" type="radio" :value="dates.today" v-model="selected_start_date"' +
	                    ' id="start-now" name="start-now" @click="update_start_date">' +
	                    '<label class="width-90-per sm-text" for="start-now">' + translator.get('start-now-label') + '</label>' +
                        '<span class="subscription-comparison-info upgrade-info-m2m">' + translator.get('upgrade-cancels-immediately-notice') + '</span>' +
						'<span class="subscription-comparison-info upgrade-info-prepaid">' + translator.get('upgrade-from-prepaid-notice-2') + '</span>' +
	                '</div>' +
	                '<div>' +
	                    '<input class="float-left" type="radio" :value="dates.start_date" v-model="selected_start_date"' +
	                    ' id="start-now" name="start-now" @click="update_start_date">' +
	                    '<label class="width-90-per sm-text" for="start-now" v-html="payment_translation"></label>' +
	                '</div>' + //TODO: add the other message types IE prepaid upgrade alert
	          '</div>',
	data: function(){
		return {
			data_bus: subscription_data_bus,
			subscription: subscription_data_bus.subscription,
			user_subscriptions: [],
			subscription_names: subscription_display_names,
			selected_start_date: ''
		}
	},
	created: function() {
		this.user_subscriptions = user_subscription_data;
	},
	computed: {
		current_subscription: function() {
			if(this.user_subscriptions.length > 0)
			{
				return this.user_subscriptions[0];
			}
			
			return {};
		},
		dates: function() {
			var date = new Date(Date.now());
			var start_day = date.getDate();
			var start_year = date.getFullYear();
			//Month is 0 index
			var start_month = date.getMonth() + 1;
			var end_date = '';
			
			var today = start_month + "/" + start_day + "/" + start_year;
						
			//Make adjustments for user subscription data
			if(this.user_subscriptions.length > 0)
			{
				date = new Date(this.user_subscriptions[(this.user_subscriptions.length -1)].subscriptionEnd);
				start_day = date.getDate();
			}
			
			start_year = date.getFullYear();
			//Month is 0 index
			start_month = date.getMonth() + 1;
			
			end_date = start_month + "/" + start_day+ "/" + (start_year + 1);
						
			return {
				end_date: end_date,
				start_date: start_month + "/" + start_day + "/" + start_year,
				today: today
			};
		},
		final_subscription: function() {
			if(this.user_subscriptions.length > 1)
			{
				return this.user_subscriptions[this.user_subscriptions.length -1];
			}
			
			return {};
		},
		subscriptions_info_table: function() {
			
			if(this.user_subscriptions.length < 1)
			{
				return {};
			}
			
			var subscriptions_table = {
				headers: [
						translator.get('current-subs-header-1'),
						translator.get('current-subs-header-2'),
						translator.get('current-subs-header-3')
					],
				rows: [
					//columns data as arrays
				]
			};
			
			for(var i = 0; i < this.user_subscriptions.length; i++)
			{
				var date = new Date(this.user_subscriptions[i].subscriptionStart);
				var start_date = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
				
				date = new Date(this.user_subscriptions[i].subscriptionEnd);
				var end_date = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
				
				subscriptions_table.rows.push([
					subscription_display_names[this.user_subscriptions[i].subscriptionType], //Subscription name
					start_date,
					end_date
				]);
			}
			
			return subscriptions_table;
		},
		payment_translation: function() {
			return translator.get('generic-start-text') + " " + this.dates.start_date + " - " + translator.get('payment-info-for-delayed-start', {'@@todays-payment': currency_format(0, currency_code, currency_symbol), '@@subscription-end-date': this.dates.start_date});
		}
	},
	methods: {
		update_start_date: function() {
			
			var today = new Date();
			var selected = new Date(this.selected_start_date);
			var type = 'now';
			
			if(today.getTime() < selected.getTime())
			{
				type = 'append';
			}
			
			this.subscription.set_start_date(this.selected_start_date, type);
			this.data_bus.update_summary();
			
		}
	}
});

Vue.component('review-confirm',{
	template:   '<div class="padding-left-sm">' +
					'<div class="margin-bottom-sm">' + translator.get('review-and-confirm-subtext') + '</div>' +
	                '<div v-for="key in purchase_summary.keys">' +
	                    '<span class="bold">{{key}}: </span>' + '<span v-html="purchase_summary[key]"></span>' +
	                '</div>' +
					'<div class="margin-top-md text-center">' +
	                    translator.get('todays-payment-label') + '<span v-html="summary.todays_payment" class="silhouette-blue-text bold lg-text"></span>' +
	                    '<button @click="submit_purchase" class="btn btn-primary margin-left-xs">Purchase</button>' +
	                '</div>' +
	            '</div>',
	data: function() {
		return {
			data_bus: subscription_data_bus.subscription,
			summary: subscription_data_bus.summary
		}
	},
	computed: {
		purchase_summary: function() {
			var summary_obj = {};
			
			var billing_option = "Monthly (" + currency_symbol + this.data_bus.todays_payment + " today and " + (this.data_bus.duration -1) + " months at " + currency_symbol + this.data_bus.monthly_payment + " each)";
			
			if(this.data_bus.prepaid)
			{
				billing_option = 'One time payment of ' + this.summary.todays_payment + ' today';
			}
			
			summary_obj.keys = ['Subscription Type', 'Subscription Length', 'Monthly Credits', 'Billing Option', 'Card Info', 'Start Date'];
			summary_obj['Subscription Type'] = this.summary.name;
			summary_obj['Subscription Length'] = this.data_bus.duration + " Months";
			summary_obj['Monthly Credits'] = currency_symbol + this.data_bus.allowance;
			summary_obj['Billing Option'] = billing_option;
			summary_obj['Card Info'] = "************" + this.summary.selected_card_last_four;
			summary_obj['Start Date'] = this.summary.start_date;
			
			return summary_obj;
		}
	},
	methods: {
		submit_purchase: function() {
			this.data_bus.submit_purchase()
		}
	}
});

Vue.component('simple-table',{
	template: '<table>' +
	                '<thead>' +
	                    '<tr>' +
	                        '<th class="sm-text" v-for="column_header in table_data.headers">{{column_header}}</th>' +
	                    '</tr>' +
	                '</thead>' +
                    '<tbody>' +
	                    '<tr v-for="row in table_data.rows">' +
	                        '<td class="sm-text" v-for="column in row">{{column}}</td>' +
	                    '</tr>' +
                    '</tbody>' +
	          '</table>',
	data: function(){
		return {};
	},
	props: {
		table_data: {
			type: Object,
			required: true
		}
	}
});

new Vue({
	el: "#app"
});